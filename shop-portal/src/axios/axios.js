import Vue from 'vue'
import axios from 'axios'
// import qs from 'qs'
import {
  Message,
  Loading
} from 'element-ui'
// 导入NProgress, 包对应的JS和CSS
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
// 响应时间
axios.defaults.timeout = 5 * 1000

// 配置请求头
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8'

// 静态资源
Vue.prototype.$static = ''

// 配置接口地址
// axios.defaults.baseURL = 'http://localhost:8881/'

// 环境的切换
if (process.env.NODE_ENV === 'development') {
  axios.defaults.baseURL = 'http://localhost:8881/'
} else if (process.env.NODE_ENV === 'debug') {
  axios.defaults.baseURL = 'http://localhost:8881/'
} else if (process.env.NODE_ENV === 'production') {
  axios.defaults.baseURL = 'http://localhost:8881/'
}

var loadingInstance
// 请求拦截器
axios.interceptors.request.use(
  config => {
    NProgress.start()
    // 为请求头对象，添加token验证的 Authorization 字段
    config.headers.Authorization = window.sessionStorage.getItem('token')
    loadingInstance = Loading.service({
      lock: true,
      text: '数据加载中，请稍等',
      spinner: 'el-icon-loading',
      background: 'rgba(0, 0, 0, 0.7)'
    })
    // 设置数据格式
    config.data = JSON.stringify(config.data)
    // 返回配置对象
    return config
  },
  err => {
    loadingInstance.close()
    Message.error('请求错误')
    return Promise.reject(err)
  }
)
// 返回状态判断
axios.interceptors.response.use(
  res => {
    loadingInstance.close()
    NProgress.done()
    return res
  },
  err => {
    loadingInstance.close()
    NProgress.done()
    Message.error('请求失败，请稍后再试')
    return Promise.reject(err)
  }
)

// 发送请求
export function post (url, params) {
  return new Promise((resolve, reject) => {
    axios
      .post(url, params)
      .then(
        res => {
          resolve(res.data)
        },
        err => {
          reject(err.data)
        }
      )
      .catch(err => {
        reject(err.data)
      })
  })
}

export function get (url, params) {
  return new Promise((resolve, reject) => {
    axios
      .get(url, {
        params: params
      })
      .then(res => {
        resolve(res.data)
      })
      .catch(err => {
        console.log(err)
      })
  })
}
