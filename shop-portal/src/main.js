// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// import axios from 'axios'
import api from './api/index'

// 导入自定义模板Layout
import HomeLayout from '@/layouts/Home'
import DefaultLayout from '@/layouts/Default'
import LoginRigesterLayout from '@/layouts/LoginRigester'
import PersonalShop from '@/layouts/PersonalShop'
import EnterpriseShop from '@/layouts/EnterpriseShop'
import CartLayout from '@/layouts/Cart'
import UserHome from '@/layouts/UserHome'
import Search from '@/layouts/Search'
import Develop from '@/layouts/Develop'

// 引入element-ui配置信息
import '@/plugins/element'
// 引入element-ui样式文件
import 'element-ui/lib/theme-chalk/index.css'
// 导入全局样式
import '@/assets/css/global.css'

// 导入字体图标
import '@/assets/fonts/iconfont.css'

// 配置请求根路径，包括本机地址、远程地址
// axios.defaults.baseURL = 'http://localhost:8881/'

Vue.config.productionTip = false

// 将axios挂载到Vue实例，后面可通过this调用
// Vue.prototype.$http = axioss
Vue.prototype.$api = api
// 将layout模板导入导入vue中
Vue.component('default-layout', DefaultLayout)
Vue.component('home-layout', HomeLayout)
Vue.component('loginRigester-layout', LoginRigesterLayout)
Vue.component('personalShop-layout', PersonalShop)
Vue.component('enterpriseShop-layout', EnterpriseShop)
Vue.component('cart-layout', CartLayout)
Vue.component('userHome-layout', UserHome)
Vue.component('search-layout', Search)
Vue.component('develop-layout', Develop)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
