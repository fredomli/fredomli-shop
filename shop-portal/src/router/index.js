import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/pages/Home' // 商城首页
import Items from '@/pages/Items' // 商品分类
import Login from '@/components/Login' // 登录服务
import Register from '@/pages/Register' // 注册服务
import Cart from '@/pages/Cart' // 购物车服务
import SearchList from '@/pages/SearchList' // 搜索服务
import ProductDetailsEnter from '@/pages/ProductDetailsEnter' // 商品详情
import ProductDetailsPersonal from '@/pages/ProductDetailsPersonal' // 商品详情
import UserHome from '@/pages/UserHome' // 用户中心
import Order from '@/pages/Order' // 订单服务
import Shops from '@/pages/Shops' // 商家模块
import Message from '@/pages/Message' // 消息中心
import Pay from '@/pages/Pay' // 支付服务
import Favorite from '@/pages/Favorite' // 收藏夹
import Footprint from '@/pages/Footprint' // 足迹
import ServerHome from '@/pages/ServerHome' // 服务中心-客服
import ConsumerService from '@/pages/ConsumerService' // 消费者服务
import ShopService from '@/pages/ShopService' // 商家服务

import ShopServiceCenter from '@/pages/ShopServiceCenter' // 商家服务中心
import OpenShop from '@/pages/OpenShop' // 开店服务
import OpenShopEnterprise from '@/pages/OpenShopEnterprise' // 企业入驻
import OpenShopPersonal from '@/pages/OpenShopPersonal' // 个人入驻

import UserWelcome from '@/components/userhome/UserWelcome' // 用户中心模块
import UserIndex from '@/components/userhome/UserIndex' // 用户中心模块
import UserCart from '@/components/userhome/UserCart' // 用户中心模块
import UserBaobe from '@/components/userhome/UserBaobe' // 用户中心模块
import UserFav from '@/components/userhome/UserFav' // 用户中心模块
import UserOrder from '@/components/userhome/UserOrder' // 用户中心模块
import UserShop from '@/components/userhome/UserShop' // 用户中心模块
import UserFoot from '@/components/userhome/UserFoot' // 用户中心模块
import UserMessage from '@/components/userhome/UserMessage' // 用户中心模块
import UserAutor from '@/components/userhome/UserAutor' // 用户中心模块

Vue.use(Router)

const routes = [
  { path: '*', redirect: '/home' },
  { path: '/', redirect: '/home' },
  { path: '/login', component: Login, meta: { layout: 'loginRigester' } },
  { path: '/home', component: Home, meta: { layout: 'home' } },
  { path: '/items', component: Items, meta: { layout: 'develop' } },
  { path: '/register', name: 'register', component: Register, meta: { layout: 'loginRigester' } },
  { path: '/cart', component: Cart, meta: { layout: 'cart' } },
  { path: '/search', name: 'search', component: SearchList, meta: { layout: 'search' } },
  { path: '/product-details', name: 'dtails-personal', component: ProductDetailsPersonal, meta: { layout: 'personalShop' } },
  { path: '/product-details-enter', name: 'details-enter', component: ProductDetailsEnter, meta: { layout: 'enterpriseShop' } },
  {
    path: '/user-home',
    name: 'user-home',
    component: UserHome,
    meta: { layout: 'userHome' },
    redirect: '/Welcome',
    children: [
      { path: '/user-home', component: UserWelcome, meta: { layout: 'userHome' } },
      { path: 'user-index', component: UserIndex, meta: { layout: 'userHome' } },
      { path: 'user-cart', component: UserCart, meta: { layout: 'userHome' } },
      { path: 'user-order', component: UserOrder, meta: { layout: 'userHome' } },
      { path: 'user-baobe', component: UserBaobe, meta: { layout: 'userHome' } },
      { path: 'user-shop', component: UserShop, meta: { layout: 'userHome' } },
      { path: 'user-fav', component: UserFav, meta: { layout: 'userHome' } },
      { path: 'user-foot', component: UserFoot, meta: { layout: 'userHome' } },
      { path: 'user-message', component: UserMessage, meta: { layout: 'userHome' } },
      { path: 'user-autor', component: UserAutor, meta: { layout: 'userHome' } }
    ]
  },
  { path: '/order', name: 'order', component: Order, meta: { layout: 'develop' } },
  { path: '/shops', name: 'shops', component: Shops, meta: { layout: 'develop' } },
  { path: '/message', name: 'message', component: Message, meta: { layout: 'develop' } },
  { path: '/pay', name: 'pay', component: Pay, meta: { layout: 'default' } },
  { path: '/favorite', name: 'favorite', component: Favorite, meta: { layout: 'develop' } },
  { path: '/footprint', name: 'footprint', component: Footprint, meta: { layout: 'develop' } },
  { path: '/server-home', name: 'server-home', component: ServerHome },
  { path: '/consumer-service', name: 'consumer-service', component: ConsumerService, meta: { layout: 'develop' } },
  { path: '/shop-service', name: 'shop-service', component: ShopService, meta: { layout: 'develop' } },
  { path: '/shop-service-center', name: 'shop-service-center', component: ShopServiceCenter, meta: { layout: 'develop' } },
  { path: '/open-shop', name: 'open-shop', component: OpenShop, meta: { layout: 'develop' } },
  { path: '/open-shop-enterprise', name: 'open-shop-enterprise', component: OpenShopEnterprise, meta: { layout: 'develop' } },
  { path: '/open-shop-personal', name: 'open-shop-personal', component: OpenShopPersonal, meta: { layout: 'develop' } }
]

// 将配置好的路由路径进行配置  base: '/fredomli-pages/'
const router = new Router({ routes, mode: 'history' })

// 挂载路由导航守卫，to表示将要访问的路径，from表示从哪里来，next是下一步需要做的操作 next('/login') 强制跳转登录页面
router.beforeEach((to, from, next) => {
  // 放行页面
  if (to.path === '/login') return next()
  if (to.path === '/home') return next()
  if (to.path === '/product-details') return next()
  if (to.path === '/register') return next()
  // 获取 token
  const token = window.sessionStorage.getItem('token')

  // 没有 token强制跳转到登录页面
  if (!token) return next('/login')
  next()
})

export default router
