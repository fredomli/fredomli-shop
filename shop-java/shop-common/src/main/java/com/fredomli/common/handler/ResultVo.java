package com.fredomli.common.handler;

import com.fredomli.common.response.Result;

import javax.xml.crypto.Data;
import java.util.Map;

/**
 * @author lishuang
 * @calssName ResultVo
 * @descriptions
 * @date 2020/12/31 12:37
 */
public class ResultVo {
    public static Result success(Map<String,Object> data) {
        return Result.ok().data(data);
    }

    public static Result success(String result , Object data) {
        return Result.ok().data(result,data);
    }
    public static Result success() {
        return Result.ok();
    }
    public static Result error(){
        return Result.error();
    }
    public static Result error(Integer code,String msg) {
        return Result.error().code(code).message(msg);
    }
}
