package com.fredomli.common.handler;

import com.fredomli.common.response.Result;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author lishuang
 * @calssName GlobalExceptionHandler
 * @description
 * @date 2020/12/31 12:34
 */
@ControllerAdvice
@Api("统一异常捕获")
public class GlobalExceptionHandler {
    @ExceptionHandler(value = SellException.class)
    @ResponseBody
    public Result handlerSellerException(SellException e){
        return ResultVo.error(e.getCode(),e.getMessage());
    }
}
