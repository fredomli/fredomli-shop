package com.fredomli.common.handler;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;

@Getter
@ApiModel("异常信息统一枚举定义")
public enum ResultEnum {
    SUCCESS(20000, "成功"),
    USER_LOGIN_ERROR(20001,"用户登录失败"),
    USER_LOGIN_SUCCESS(20000, "用户登录成功"),
    ;
    @ApiModelProperty("异常码")
    private Integer code;
    @ApiModelProperty("异常信息")
    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
