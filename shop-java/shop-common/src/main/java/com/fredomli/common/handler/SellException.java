package com.fredomli.common.handler;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author lishuang
 * @calssName SellException
 * @description
 * @date 2020/12/31 12:24
 */
@Data
@ApiModel("统一异常处理")
public class SellException extends RuntimeException {
    private Integer code;
    private String message;
    public SellException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());
        this.code = resultEnum.getCode();
    }

    public SellException(Integer code,String message) {
        this.code = code;
        this.message = message;
    }
}
