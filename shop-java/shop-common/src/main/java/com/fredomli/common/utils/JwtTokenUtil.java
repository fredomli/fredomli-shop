package com.fredomli.common.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lishuang
 * @calssName JwtTokenUtil
 * @description
 * @date 2021/1/3 22:01
 */
public class JwtTokenUtil {
    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer";
    private static final String SECRET = "jwtsecretdemo";
    private static final String ISS = "echisan";


    // 过期时间 3600秒
    private static final int EXPIRATION = 60;
    // 选择了记住我之后的过期时间为7天
    private static final int EXPIRATION_REMEMBER = 60*24*7;

    // 创建token
    public static String createToken(String username, boolean isRememberMe) {
        int expiration = isRememberMe ? EXPIRATION_REMEMBER : EXPIRATION;
        Map<String, Object> map = new HashMap<>();
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.MINUTE,expiration);
        String token = JWT.create()
                .withHeader(map)
                .withClaim("username",username)
                .withExpiresAt(instance.getTime())
                .sign(Algorithm.HMAC256(SECRET));
        return token;
    }

    public static String isVerify(String token){
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET);
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT jwt = verifier.verify(token);
            String username = jwt.getClaim("username").asString();
            return username;
        } catch (Exception e){
            return "20001";
        }
    }
}
