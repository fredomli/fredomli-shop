package com.fredomli.common.response;

public interface ResultCode {
    public static Integer SUCCESS = 200;
    public static Integer ERROR = 20001;
}
