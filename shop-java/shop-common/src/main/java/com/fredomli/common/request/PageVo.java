package com.fredomli.common.request;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

/**
 * @author lishuang
 * @calssName PageVo
 * @description
 * @date 2021/1/4 0:14
 */
@Api(value = "统一分页参数接收",description = "分页参数统一接收")
@Data
public class PageVo {
    @ApiModelProperty(value = "当前页",example = "1")
    private int page;
    @ApiModelProperty(value = "每页数据条数",example = "5")
    private int pageSize;
}
