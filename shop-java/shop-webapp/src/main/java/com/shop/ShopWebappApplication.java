package com.shop;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

// (exclude = {SecurityAutoConfiguration.class})
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@EnableSwagger2
@MapperScan("com.shop.webapp.mapper")
public class ShopWebappApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShopWebappApplication.class, args);
    }
}
