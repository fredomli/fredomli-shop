package com.shop.webapp.controller;


import com.fredomli.common.response.Result;
import com.shop.webapp.entity.ApProductDetail;
import com.shop.webapp.mapper.ApProductDetailMapper;
import com.shop.webapp.service.ApProductDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@RestController
@RequestMapping("/webapp/ap-product-detail")
@Api(value = "商品详情模块", tags = "商品详情接口")
public class ApProductDetailController {
    @Resource
    private ApProductDetailMapper productDetailMapper;

    @Autowired
    private ApProductDetailService productDetailService;

    @ApiOperation("获取商品详情记录")
    @GetMapping("get-product-detail")
    public Result getProductDetailById(@RequestParam(value = "商品id", required = true)String id) {
        ApProductDetail apProductDetail =productDetailMapper.selectById(id);
        return Result.ok().data("apProductDetail",apProductDetail);
    }
}

