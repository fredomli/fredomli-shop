package com.shop.webapp.service.impl;

import com.shop.webapp.entity.ApProduct;
import com.shop.webapp.mapper.ApProductMapper;
import com.shop.webapp.service.ApProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8 服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class ApProductServiceImpl extends ServiceImpl<ApProductMapper, ApProduct> implements ApProductService {

}
