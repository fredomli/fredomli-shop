package com.shop.webapp.controller;


import com.fredomli.common.response.Result;
import com.shop.webapp.entity.ApBrand;
import com.shop.webapp.mapper.ApBrandMapper;
import com.shop.webapp.service.ApBrandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 品牌表，一个品牌下有多个商品（ap_product），一对多关系 前端控制器
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@RestController
@RequestMapping("/webapp/ap-brand")
@Api(value = "品牌模块", tags = "品牌模块接口")
public class ApBrandController {
    @Resource
    private ApBrandMapper brandMapper;

    @Autowired
    private ApBrandService brandService;

    @ApiOperation("获取品牌列表")
    @GetMapping("get-brand-list")
    public Result getBrandList() {
        List<ApBrand> apBrands = brandMapper.selectList(null);
        return Result.ok().data("apBrands",apBrands);
    }

    @ApiOperation("获取单条品牌记录")
    @GetMapping("get-one-brand")
    public Result getOneBrand(@RequestParam String id) {
        ApBrand oneBrand = brandMapper.selectById(id);
        return Result.ok().data("oneBrand",oneBrand);
    }

    @ApiOperation("添加单条品牌记录")
    @PostMapping("add-brand")
    public Result addBrand() {
        return Result.ok().data("addBrand","addBrand");
    }

    @ApiOperation("更新品牌记录")
    @PutMapping("update-brand")
    public Result updateBrand() {
        return Result.ok().data("updateBrand","updateBrand");
    }

    @ApiOperation("删除品牌记录")
    @DeleteMapping("delete-brand")
    public Result deleteBrand() {
        return Result.ok().data("deleteBrand","deleteBrand");
    }

}

