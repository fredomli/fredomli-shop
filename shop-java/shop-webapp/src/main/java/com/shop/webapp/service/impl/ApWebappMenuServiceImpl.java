package com.shop.webapp.service.impl;

import com.shop.webapp.entity.ApWebappMenu;
import com.shop.webapp.mapper.ApWebappMenuMapper;
import com.shop.webapp.service.ApWebappMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 门户菜单 服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class ApWebappMenuServiceImpl extends ServiceImpl<ApWebappMenuMapper, ApWebappMenu> implements ApWebappMenuService {

}
