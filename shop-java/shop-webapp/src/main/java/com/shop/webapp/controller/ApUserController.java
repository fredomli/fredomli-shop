package com.shop.webapp.controller;


import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fredomli.common.handler.ResultEnum;
import com.fredomli.common.request.PageVo;
import com.fredomli.common.response.Result;
import com.fredomli.common.utils.JwtTokenUtil;
import com.shop.webapp.entity.ApUser;
import com.shop.webapp.mapper.ApUserMapper;
import com.shop.webapp.service.ApUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import javafx.beans.binding.StringBinding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@RestController
@RequestMapping("/webapp/ap-user")
@Api(value = "用户模块", tags = "用户模块接口")
public class ApUserController {
    Logger log = LoggerFactory.getLogger(ApUserController.class);
    @Resource
    private ApUserMapper userMapper;

    @Autowired
    private ApUserService userService;

    @ApiOperation("获取用户列表")
    @GetMapping("get-user-list")
    public Result getUserList() {
        return Result.ok().data("getUserList", "getUserList");
    }

    @ApiOperation("获取单条用户数据")
    @GetMapping("get-one-user")
    public Result getOneUser(
            @ApiParam(value = "用户id", required = true) String id) {
        return Result.ok().data("getOneUser", "getOneUser");
    }

    @ApiOperation("添加用户数据")
    @PostMapping("add-one-user")
    public Result addOneUser(
            @ApiParam(value = "用户对象") ApUser user) {
        return Result.ok().data("addOneUser", "addOneUser");
    }

    @ApiOperation("修改用户数据")
    @PutMapping("update-one-user")
    public Result updateOneUser(
            @ApiParam(value = "用户对象") ApUser user) {
        return Result.ok().data("updateOneUser", "updateOneUser");
    }

    @ApiOperation("删除用户数据")
    @DeleteMapping("delete-one-user")
    public Result deleteOneUser(
            @ApiParam(value = "用户id", required = true) String id) {
        return Result.ok().data("deleteOneUser", "deleteOneUser");
    }

    @ApiOperation("用户登录")
    @PostMapping("user-login")
    public Result userLogin(
            @RequestBody() Map<String, String> user) {
        log.info(user.get("username"));
        log.info(user.get("password"));
        QueryWrapper<ApUser> wrapper = new QueryWrapper<>();
        wrapper.eq("username", user.get("username"));
        wrapper.eq("password", user.get("password"));
        ApUser apUser = userMapper.selectOne(wrapper);
        if (apUser != null) {
            String token = JwtTokenUtil.createToken(apUser.getUsername(), true);
            return Result.ok().data("user", apUser).data("token", token);
        }
        return Result.error();
    }

    @ApiOperation("用户注册")
    @PostMapping("user-register")
    public Result userRegister(
            @RequestBody ApUser users) {
        return Result.ok().data("userRegister", "userRegister");
    }

    @ApiOperation("用户数据分页查询")
    @PostMapping("get-Info-pages")
    public Result getInfoPages(
            @RequestBody PageVo pageVo
    ) {
        int page = pageVo.getPage();
        int pageSize = pageVo.getPageSize();
        Page<ApUser> mypage = new Page<>(page, pageSize);
        IPage<ApUser> list = userMapper.selectPage(mypage, null);
        Object object = JSONArray.toJSON(list.getRecords());
        return Result.ok()
                .data("data", object)
                .data("page",list.getPages())
                .data("total",list.getTotal())
                .data("current",list.getCurrent())
                .data("size",list.getSize());
    }

}

