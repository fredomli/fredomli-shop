package com.shop.webapp.controller;


import com.fredomli.common.response.Result;
import com.shop.webapp.entity.ApOrderDetail;
import com.shop.webapp.mapper.ApOrderDetailMapper;
import com.shop.webapp.service.ApOrderDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 订单详情表 前端控制器
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@RestController
@RequestMapping("/webapp/ap-order-detail")
@Api(value = "订单详情模块", tags = "订单详情接口")
public class ApOrderDetailController {
    @Resource
    private ApOrderDetailMapper orderDetailMapper;

    @Autowired
    private ApOrderDetailService orderDetailService;

    @ApiOperation("查看订单详情列表")
    @GetMapping("order-detail-list")
    public Result orderDetailList(){
        List<ApOrderDetail> apOrderDetails = orderDetailMapper.selectList(null);
        return Result.ok().data("apOrderDetails",apOrderDetails);
    }

    @ApiOperation("查看订单详情")
    @GetMapping("find-order-detail")
    public Result findOrderDetail(@RequestParam(value = "订单id",required = true) String id){
        ApOrderDetail apOrderDetail = orderDetailMapper.selectById(id);
        return Result.ok().data("apOrderDetail",apOrderDetail);
    }
}

