package com.shop.webapp.service;

import com.shop.webapp.entity.MpRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface MpRoleMenuService extends IService<MpRoleMenu> {

}
