package com.shop.webapp.service;

import com.shop.webapp.entity.ApSpecGroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 规格参数的分组表，每个商品分类下有多个规格参数组 服务类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApSpecGroupService extends IService<ApSpecGroup> {

}
