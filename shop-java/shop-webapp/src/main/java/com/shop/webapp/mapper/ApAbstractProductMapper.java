package com.shop.webapp.mapper;

import com.shop.webapp.entity.ApAbstractProduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * spu表，该表描述的是一个抽象性的商品，比如 iphone8 Mapper 接口
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApAbstractProductMapper extends BaseMapper<ApAbstractProduct> {

}
