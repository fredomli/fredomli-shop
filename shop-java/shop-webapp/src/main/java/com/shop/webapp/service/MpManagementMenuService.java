package com.shop.webapp.service;

import com.shop.webapp.entity.MpManagementMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 运营管理菜单 服务类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface MpManagementMenuService extends IService<MpManagementMenu> {

}
