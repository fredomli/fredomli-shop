package com.shop.webapp.service.impl;

import com.shop.webapp.entity.ApSpecParam;
import com.shop.webapp.mapper.ApSpecParamMapper;
import com.shop.webapp.service.ApSpecParamService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 规格参数组下的参数名 服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class ApSpecParamServiceImpl extends ServiceImpl<ApSpecParamMapper, ApSpecParam> implements ApSpecParamService {

}
