package com.shop.webapp.mapper;

import com.shop.webapp.entity.ApStock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 库存表，代表库存，秒杀库存等信息 Mapper 接口
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApStockMapper extends BaseMapper<ApStock> {
    public List<ApStock> productAndStockList();
}
