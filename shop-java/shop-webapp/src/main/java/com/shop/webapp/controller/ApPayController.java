package com.shop.webapp.controller;


import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.fredomli.common.response.Result;
import com.fredomli.common.vo.OrderPojo;
import com.shop.config.AlipayConfig;
import com.shop.webapp.entity.ApPay;
import com.shop.webapp.mapper.ApPayMapper;
import com.shop.webapp.service.ApPayService;
import com.sun.org.apache.xpath.internal.operations.Or;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@RestController
@RequestMapping("/webapp/ap-pay")
@Api(value = "支付模块", tags = "支付模块接口")
public class ApPayController {
    @Resource
    private ApPayMapper payMapper;

    @Autowired
    private ApPayService payService;

    @ApiOperation("获取支付列表")
    @GetMapping("pay-list")
    public Result getOrderList() {
        List<ApPay> apPays = payMapper.selectList(null);
        return Result.ok().data("paylist",apPays);
    }

    @ApiOperation("获取单条支付记录")
    @GetMapping("get-one-pay")
    public Result getOneOrder(@RequestParam String id) {
        ApPay apPay = payMapper.selectById(id);
        return Result.ok().data("apPay",apPay);
    }

    @ApiOperation("添加支付记录")
    @PostMapping("add-pay")
    public Result addPay() {
        return Result.ok().data("pay","addPay");
    }

    @ApiOperation("修改支付记录")
    @PutMapping("update-pay")
    public Result updatePay() {
        return Result.ok().data("pay","updatepay");
    }


    @ApiOperation("删除支付记录")
    @DeleteMapping("delete-pay")
    public Result deletePay(@ApiParam("订单id") String id) {
        return Result.ok().data("pay","deletepay");
    }

    @ApiOperation("商城支付-支付宝支付")
    @PostMapping("to-pay")
    public Result toPay(@RequestBody List<OrderPojo> orderPojoList) {
        System.out.println(orderPojoList);
        return Result.ok().data("paystatus", "success");
    }

    @ApiOperation("支付测试")
    @PostMapping("pay-test")
    public String payTest() throws Exception {
        System.out.println("pay");
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);
        //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(AlipayConfig.return_url);
        alipayRequest.setNotifyUrl(AlipayConfig.notify_url);
        try {
            alipayRequest.setBizContent("{\"out_trade_no\":\"" + 1682787125 + "\","
                    + "\"total_amount\":\"" + 100 + "\","
                    + "\"subject\":\"" + "测试支付" + "\","
                    + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
            // 请求
            String result;
            result = alipayClient.pageExecute(alipayRequest).getBody();
            System.out.println("*********************\n返回结果为：" + result);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @ApiOperation("支付回调")
    @GetMapping("back")
    public Result notifyBack() {
        return Result.ok().data("notifyBack", "notifyBack");
    }
}

