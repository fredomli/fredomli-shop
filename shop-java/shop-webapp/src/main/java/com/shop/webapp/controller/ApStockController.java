package com.shop.webapp.controller;


import com.fredomli.common.response.Result;
import com.shop.webapp.entity.ApStock;
import com.shop.webapp.mapper.ApStockMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 库存表，代表库存，秒杀库存等信息 前端控制器
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@RestController
@RequestMapping("/webapp/ap-stock")
@Api(value = "库存模块", tags = "商品库存接口")
public class ApStockController {
    @Resource
    private ApStockMapper stockMapper;

    @ApiOperation("商品库存列表")
    @GetMapping("get-stock-list")
    public Result getStockList() {
        List<ApStock> apStocks = stockMapper.selectList(null);
        return Result.ok().data("apStocks",apStocks);
    }
}

