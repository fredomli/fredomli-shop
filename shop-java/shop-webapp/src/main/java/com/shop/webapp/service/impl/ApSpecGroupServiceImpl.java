package com.shop.webapp.service.impl;

import com.shop.webapp.entity.ApSpecGroup;
import com.shop.webapp.mapper.ApSpecGroupMapper;
import com.shop.webapp.service.ApSpecGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 规格参数的分组表，每个商品分类下有多个规格参数组 服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class ApSpecGroupServiceImpl extends ServiceImpl<ApSpecGroupMapper, ApSpecGroup> implements ApSpecGroupService {

}
