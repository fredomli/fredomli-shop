package com.shop.webapp.service;

import com.shop.webapp.entity.ApOrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单详情表 服务类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApOrderDetailService extends IService<ApOrderDetail> {

}
