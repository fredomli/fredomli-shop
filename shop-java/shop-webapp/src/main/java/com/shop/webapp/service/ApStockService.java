package com.shop.webapp.service;

import com.shop.webapp.entity.ApStock;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 库存表，代表库存，秒杀库存等信息 服务类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApStockService extends IService<ApStock> {

}
