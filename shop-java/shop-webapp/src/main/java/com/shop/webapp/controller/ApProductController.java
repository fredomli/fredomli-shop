package com.shop.webapp.controller;


import com.fredomli.common.response.Result;
import com.shop.webapp.entity.ApProduct;
import com.shop.webapp.entity.ApUser;
import com.shop.webapp.mapper.ApProductMapper;
import com.shop.webapp.service.ApProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8 前端控制器
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@RestController
@RequestMapping("/webapp/ap-product")
@Api(value = "商品模块",tags = "商品模块接口")
public class ApProductController {
    @Resource
    private ApProductMapper productMapper;

//    @Autowired
//    private ApProductService productService;

    @ApiOperation("获取商品列表")
    @GetMapping("get-product-items")
    public Result getProductItems() {
        return Result.ok().data("getProductItems","getProductItems");
    }


    @ApiOperation("商品分页展示")
    @GetMapping("get-product-page")
    public Result getProductPage() {
        return Result.ok().data("getProductPage","getProductPage");
    }

    @ApiOperation("获取单条商品记录")
    @GetMapping("get-one-product")
    public Result getProductById(@RequestParam String id) {
        ApProduct apProduct = productMapper.selectById(id);
        return Result.ok().data("oneProduct", apProduct);
    }

    @ApiOperation("获取单条商品记录")
    @PostMapping("add-product")
    public Result addProduct() {
        return Result.ok().data("addProduct","addProduct");
    }

    @ApiOperation("修改商品记录")
    @PutMapping("update-product")
    public Result updateProduct() {
        return Result.ok().data("updateProduct","updateProduct");
    }

    @ApiOperation("删除商品记录")
    @DeleteMapping("delete-product")
    public Result deleteProduct() {
        return Result.ok().data("deleteProduct","deleteProduct");
    }

    @ApiOperation("商城首页-发现好货")
    @GetMapping("find-good-product")
    public Result findGoodProduct() {
        List<ApProduct> goodProduct = productMapper.findGoodProduct();
        return Result.ok().data("goodProduct",goodProduct);
    }

    @ApiOperation("商城首页-热门商品")
    @GetMapping("find-red-product")
    public Result findRedProduct() {
        List<ApProduct> redProduct = productMapper.findRedProduct();
        return Result.ok().data("redProduct",redProduct);
    }

    @ApiOperation("商城首页-猜你喜欢")
    @GetMapping("find-like-product")
    public Result findLikeProduct() {
        List<ApProduct> likeProduct = productMapper.findlikeProduct();
        return Result.ok().data("apProducts",likeProduct);
    }

    @ApiOperation("商城首页-走马灯")
    @GetMapping("roll-show-product")
    public Result rollShowProduct() {
        List<ApProduct> showProduct = productMapper.rollShowProduct();
        return Result.ok().data("rollShowProduct",showProduct);
    }

    @ApiOperation("商城首页-商品展示")
    @GetMapping("double-show-product")
    public Result doubleShowProduct() {
        return Result.ok().data("doubleShowProduct","doubleShowProduct");
    }
}

