package com.shop.webapp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 库存表，代表库存，秒杀库存等信息
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ApStock对象", description="库存表，代表库存，秒杀库存等信息")
public class ApStock implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "库存对应的商品sku id")
    @TableId(value = "sku_id", type = IdType.AUTO)
    private Long skuId;

    @ApiModelProperty(value = "可秒杀库存")
    private Integer seckillStock;

    @ApiModelProperty(value = "秒杀总数量")
    private Integer seckillTotal;

    @ApiModelProperty(value = "库存数量")
    private Integer stock;


}
