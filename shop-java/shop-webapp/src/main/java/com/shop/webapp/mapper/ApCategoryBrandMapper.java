package com.shop.webapp.mapper;

import com.shop.webapp.entity.ApCategoryBrand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品分类和品牌的中间表，两者是多对多关系 Mapper 接口
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApCategoryBrandMapper extends BaseMapper<ApCategoryBrand> {

}
