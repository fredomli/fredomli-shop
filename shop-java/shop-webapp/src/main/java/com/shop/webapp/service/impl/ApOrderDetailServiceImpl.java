package com.shop.webapp.service.impl;

import com.shop.webapp.entity.ApOrderDetail;
import com.shop.webapp.mapper.ApOrderDetailMapper;
import com.shop.webapp.service.ApOrderDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单详情表 服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class ApOrderDetailServiceImpl extends ServiceImpl<ApOrderDetailMapper, ApOrderDetail> implements ApOrderDetailService {

}
