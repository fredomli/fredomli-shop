package com.shop.webapp.mapper;

import com.shop.webapp.entity.ApOrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单详情表 Mapper 接口
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApOrderDetailMapper extends BaseMapper<ApOrderDetail> {

}
