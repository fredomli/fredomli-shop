package com.shop.webapp.service;

import com.shop.webapp.entity.ApAbstractProduct;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * spu表，该表描述的是一个抽象性的商品，比如 iphone8 服务类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApAbstractProductService extends IService<ApAbstractProduct> {

}
