package com.shop.webapp.mapper;

import com.shop.webapp.entity.ApSpecParam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 规格参数组下的参数名 Mapper 接口
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApSpecParamMapper extends BaseMapper<ApSpecParam> {

}
