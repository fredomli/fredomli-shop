package com.shop.webapp.service;

import com.shop.webapp.entity.ApBrand;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 品牌表，一个品牌下有多个商品（ap_product），一对多关系 服务类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApBrandService extends IService<ApBrand> {

}
