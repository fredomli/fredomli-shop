package com.shop.webapp.service.impl;

import com.shop.webapp.entity.MpManagementMenu;
import com.shop.webapp.mapper.MpManagementMenuMapper;
import com.shop.webapp.service.MpManagementMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 运营管理菜单 服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class MpManagementMenuServiceImpl extends ServiceImpl<MpManagementMenuMapper, MpManagementMenu> implements MpManagementMenuService {

}
