package com.shop.webapp.controller;


import com.fredomli.common.response.Result;
import com.shop.webapp.entity.ApCategory;
import com.shop.webapp.mapper.ApCategoryMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 商品类目表，类目和商品(ap_product)是一对多关系，类目与品牌是多对多关系 前端控制器
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@RestController
@RequestMapping("/webapp/ap-category")
@Api(value = "商品分类模块", tags = "商品分类接口")
public class ApCategoryController {
    @Resource
    private ApCategoryMapper categoryMapper;
    
    @ApiOperation("商品分类列表")
    @GetMapping("get-category-list")
    public Result getCategoryList() {
        List<ApCategory> apCategories = categoryMapper.selectList(null);
        return Result.ok().data("apCategories",apCategories);
    }

    @ApiOperation("商品单条分类记录")
    @GetMapping("get-one-category")
    public Result getOneCategory(@RequestParam String id) {
        ApCategory apCategory = categoryMapper.selectById(id);
        return Result.ok().data("apCategory",apCategory);
    }

    @ApiOperation("增加商品分类")
    @PostMapping("add-category")
    public Result addCategory() {
        return Result.ok().data("addCategory","addCategory");
    }

    @ApiOperation("修改商品分类")
    @PutMapping("update-category")
    public Result updateCategory() {
        return Result.ok().data("updateCategory","updateCategory");
    }

    @ApiOperation("删除商品分类")
    @DeleteMapping("delete-category")
    public Result deleteCategory() {
        return Result.ok().data("deleteCategory","deleteCategory");
    }
}

