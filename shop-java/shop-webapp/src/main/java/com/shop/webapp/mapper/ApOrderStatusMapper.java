package com.shop.webapp.mapper;

import com.shop.webapp.entity.ApOrderStatus;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单状态表 Mapper 接口
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApOrderStatusMapper extends BaseMapper<ApOrderStatus> {

}
