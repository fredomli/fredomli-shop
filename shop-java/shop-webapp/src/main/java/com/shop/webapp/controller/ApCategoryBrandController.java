package com.shop.webapp.controller;


import com.shop.webapp.service.ApCategoryBrandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 商品分类和品牌的中间表，两者是多对多关系 前端控制器
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@RestController
@RequestMapping("/webapp/ap-category-brand")
@Api(value = "品牌商品分类模块", tags = "品牌商品分类接口")
public class ApCategoryBrandController {

    @Autowired
    private ApCategoryBrandService categoryBrandService;

}

