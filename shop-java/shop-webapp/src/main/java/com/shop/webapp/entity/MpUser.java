package com.shop.webapp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="MpUser对象", description="")
public class MpUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "组件")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户标识")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "用户名")
    private String realname;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "注册时间")
    @TableField("inputTime")
    private Date inputtime;

    private String status;

    @ApiModelProperty(value = "生日")
    private Date birthday;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "电话")
    private String phone;

    @ApiModelProperty(value = "最后登录时间")
    @TableField("lastLoginTime")
    private Date lastlogintime;

    @ApiModelProperty(value = "token")
    @TableField("accessToken")
    private String accesstoken;

    @ApiModelProperty(value = "用户角色")
    private Integer type;

    @ApiModelProperty(value = "授权")
    @TableField("securityCode")
    private String securitycode;


}
