package com.shop.webapp.service;

import com.shop.webapp.entity.ApCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品类目表，类目和商品(ap_product)是一对多关系，类目与品牌是多对多关系 服务类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApCategoryService extends IService<ApCategory> {

}
