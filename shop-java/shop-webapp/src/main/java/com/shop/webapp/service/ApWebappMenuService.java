package com.shop.webapp.service;

import com.shop.webapp.entity.ApWebappMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 门户菜单 服务类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApWebappMenuService extends IService<ApWebappMenu> {

}
