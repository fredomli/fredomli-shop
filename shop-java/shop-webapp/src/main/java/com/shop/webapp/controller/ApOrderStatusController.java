package com.shop.webapp.controller;


import com.fredomli.common.response.Result;
import com.shop.webapp.entity.ApOrderStatus;
import com.shop.webapp.mapper.ApOrderStatusMapper;
import com.shop.webapp.service.ApOrderStatusService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 订单状态表 前端控制器
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@RestController
@RequestMapping("/webapp/ap-order-status")
@Api(value = "订单状态模块", tags = "订单状态接口")
public class ApOrderStatusController {
    @Resource
    private ApOrderStatusMapper orderStatusMapper;

    @Autowired
    private ApOrderStatusService orderStatusService;

    @ApiOperation("获取订单状态列表")
    @GetMapping("get-order-status-list")
    public Result getOrderStatusList(@RequestParam(value = "用户id",required = true) String id) {
        List<ApOrderStatus> apOrderStatuses = orderStatusMapper.selectList(null);
        return Result.ok().data("apOrderStatuses",apOrderStatuses);
    }

    @ApiOperation("获取单条订单状态记录")
    @GetMapping("get-one-order-status")
    public Result getOneOrderStatus(@RequestParam(value = "订单id",required = true) String id) {
        ApOrderStatus apOrderStatus = orderStatusMapper.selectById(id);
        return Result.ok().data("apOrderStatus",apOrderStatus);
    }

    @ApiOperation("添加订单状态记录")
    @PostMapping("add-order-status")
    public Result addOrderStatus(@ApiParam(value = "用户id",required = true) String id) {
        return Result.ok().data("addOrderStatus","addOrderStatus");
    }

    @ApiOperation("更新订单状态记录")
    @PutMapping("update-order-status")
    public Result updateOrderStatus(@ApiParam(value = "订单状态id",required = true) String id) {
        return Result.ok().data("updateOrderStatus","updateOrderStatus");
    }

    @ApiOperation("删除订单状态记录")
    @DeleteMapping("delete-order-status")
    public Result deleteOrderStatus(@ApiParam(value = "订单状态id",required = true) String id) {
        return Result.ok().data("deleteOrderStatus","deleteOrderStatus");
    }
}

