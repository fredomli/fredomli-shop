package com.shop.webapp.service.impl;

import com.shop.webapp.entity.MpRoleMenu;
import com.shop.webapp.mapper.MpRoleMenuMapper;
import com.shop.webapp.service.MpRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class MpRoleMenuServiceImpl extends ServiceImpl<MpRoleMenuMapper, MpRoleMenu> implements MpRoleMenuService {

}
