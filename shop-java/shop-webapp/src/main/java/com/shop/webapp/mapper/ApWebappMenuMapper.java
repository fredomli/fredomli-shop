package com.shop.webapp.mapper;

import com.shop.webapp.entity.ApWebappMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 门户菜单 Mapper 接口
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApWebappMenuMapper extends BaseMapper<ApWebappMenu> {

}
