package com.shop.webapp.mapper;

import com.shop.webapp.entity.MpManagementMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 运营管理菜单 Mapper 接口
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface MpManagementMenuMapper extends BaseMapper<MpManagementMenu> {

}
