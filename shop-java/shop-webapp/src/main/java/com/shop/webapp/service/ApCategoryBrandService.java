package com.shop.webapp.service;

import com.shop.webapp.entity.ApCategoryBrand;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品分类和品牌的中间表，两者是多对多关系 服务类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApCategoryBrandService extends IService<ApCategoryBrand> {

}
