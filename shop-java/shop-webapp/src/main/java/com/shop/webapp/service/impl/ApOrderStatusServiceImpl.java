package com.shop.webapp.service.impl;

import com.shop.webapp.entity.ApOrderStatus;
import com.shop.webapp.mapper.ApOrderStatusMapper;
import com.shop.webapp.service.ApOrderStatusService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单状态表 服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class ApOrderStatusServiceImpl extends ServiceImpl<ApOrderStatusMapper, ApOrderStatus> implements ApOrderStatusService {

}
