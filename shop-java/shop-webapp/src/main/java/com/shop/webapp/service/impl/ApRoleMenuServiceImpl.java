package com.shop.webapp.service.impl;

import com.shop.webapp.entity.ApRoleMenu;
import com.shop.webapp.mapper.ApRoleMenuMapper;
import com.shop.webapp.service.ApRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class ApRoleMenuServiceImpl extends ServiceImpl<ApRoleMenuMapper, ApRoleMenu> implements ApRoleMenuService {

}
