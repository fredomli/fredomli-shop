package com.shop.webapp.mapper;

import com.shop.webapp.entity.MpUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface MpUserMapper extends BaseMapper<MpUser> {

}
