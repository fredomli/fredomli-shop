package com.shop.webapp.service.impl;

import com.shop.webapp.entity.ApCategory;
import com.shop.webapp.mapper.ApCategoryMapper;
import com.shop.webapp.service.ApCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品类目表，类目和商品(ap_product)是一对多关系，类目与品牌是多对多关系 服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class ApCategoryServiceImpl extends ServiceImpl<ApCategoryMapper, ApCategory> implements ApCategoryService {

}
