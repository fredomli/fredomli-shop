package com.shop.webapp.controller;


import com.fredomli.common.response.Result;
import com.shop.webapp.service.MpUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import sun.awt.SunHints;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@RestController
@RequestMapping("/webapp/mp-user")
@Api(value = "用户管理模块" , tags = "用户管理操作接口")
public class MpUserController {

    @Autowired
    private MpUserService userService;

    @ApiOperation("获取用户列表")
    @GetMapping("get-user-list")
    public Result getUserList() {
        return Result.ok().data("userData","userlist");
    }
    @ApiOperation("获取单条用户记录")
    @GetMapping("get-one-user")
    public Result getOneUser() {
        return Result.ok().data("userData","userlist");
    }

    @ApiOperation("添加用户记录")
    @PostMapping("add-user")
    public Result addUser() {
        return Result.ok().data("userData","addUser");
    }

    @ApiOperation("修改用户记录")
    @GetMapping("update-user")
    public Result updateUser() {
        return Result.ok().data("userData","updateUser");
    }

    @ApiOperation("删除用户记录")
    @GetMapping("delete-user")
    public Result deleteUser() {
        return Result.ok().data("userData","deleteUser");
    }
}

