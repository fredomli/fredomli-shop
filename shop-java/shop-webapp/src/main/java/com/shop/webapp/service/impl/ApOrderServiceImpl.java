package com.shop.webapp.service.impl;

import com.shop.webapp.entity.ApOrder;
import com.shop.webapp.mapper.ApOrderMapper;
import com.shop.webapp.service.ApOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class ApOrderServiceImpl extends ServiceImpl<ApOrderMapper, ApOrder> implements ApOrderService {

}
