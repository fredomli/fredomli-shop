package com.shop.webapp.mapper;

import com.shop.webapp.entity.ApBrand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 品牌表，一个品牌下有多个商品（ap_product），一对多关系 Mapper 接口
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApBrandMapper extends BaseMapper<ApBrand> {

}
