package com.shop.webapp.controller;


import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fredomli.common.response.Result;
import com.fredomli.common.vo.OrderPojo;
import com.shop.config.AlipayConfig;
import com.shop.webapp.entity.ApOrder;
import com.shop.webapp.entity.ApUser;
import com.shop.webapp.mapper.ApOrderMapper;
import com.shop.webapp.service.ApOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@RestController
@RequestMapping("/webapp/ap-order")
@Api(value = "订单模块", tags = "订单模块接口")
public class ApOrderController {
    @Resource
    private ApOrderMapper orderMapper;

    @Autowired
    private ApOrderService orderService;

    @ApiOperation("获取订单列表")
    @GetMapping("list")
    public Result getOrderList(@ApiParam ApUser user) {
        System.out.println(user.toString());
        QueryWrapper<ApOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("id", user.getId());
        List<ApOrder> apOrders = orderMapper.selectList(wrapper);
        return Result.ok().data("apOrders",apOrders);
    }

    @ApiOperation("根据ID获取订单")
    @GetMapping("get-one-order")
    public Result getOneOrder(@RequestParam String id) {
        ApOrder apOrder = orderMapper.selectById(id);
        return Result.ok().data("orderlist",apOrder);
    }

    @ApiOperation("添加订单")
    @PostMapping("add-order")
    public Result addOrder() {
        return Result.ok().data("orderlist","addOrder");
    }

    @ApiOperation("修改订单")
    @PutMapping("update-order")
    public Result updateOrder() {
        return Result.ok().data("orderlist","updateorder");
    }


    @ApiOperation("删除订单")
    @DeleteMapping("delete-order")
    public Result deleteOrder(@ApiParam("订单id") String id) {
        return Result.ok().data("orderlist","deleteOrder");
    }


}

