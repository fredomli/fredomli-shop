package com.shop.webapp.service.impl;

import com.shop.webapp.entity.ApBrand;
import com.shop.webapp.mapper.ApBrandMapper;
import com.shop.webapp.service.ApBrandService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 品牌表，一个品牌下有多个商品（ap_product），一对多关系 服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class ApBrandServiceImpl extends ServiceImpl<ApBrandMapper, ApBrand> implements ApBrandService {

}
