package com.shop.webapp.mapper;

import com.shop.webapp.entity.ApCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品类目表，类目和商品(ap_product)是一对多关系，类目与品牌是多对多关系 Mapper 接口
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApCategoryMapper extends BaseMapper<ApCategory> {

}
