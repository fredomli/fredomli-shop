package com.shop.webapp.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@RestController
@RequestMapping("/webapp/mp-role-menu")
public class MpRoleMenuController {

}

