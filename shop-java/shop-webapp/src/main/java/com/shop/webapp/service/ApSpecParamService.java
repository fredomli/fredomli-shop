package com.shop.webapp.service;

import com.shop.webapp.entity.ApSpecParam;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 规格参数组下的参数名 服务类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApSpecParamService extends IService<ApSpecParam> {

}
