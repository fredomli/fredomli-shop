package com.shop.webapp.mapper;

import com.shop.webapp.entity.ApSpecGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 规格参数的分组表，每个商品分类下有多个规格参数组 Mapper 接口
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApSpecGroupMapper extends BaseMapper<ApSpecGroup> {

}
