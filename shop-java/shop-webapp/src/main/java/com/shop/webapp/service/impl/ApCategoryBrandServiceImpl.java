package com.shop.webapp.service.impl;

import com.shop.webapp.entity.ApCategoryBrand;
import com.shop.webapp.mapper.ApCategoryBrandMapper;
import com.shop.webapp.service.ApCategoryBrandService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品分类和品牌的中间表，两者是多对多关系 服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class ApCategoryBrandServiceImpl extends ServiceImpl<ApCategoryBrandMapper, ApCategoryBrand> implements ApCategoryBrandService {

}
