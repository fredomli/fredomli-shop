package com.shop.webapp.service.impl;

import com.shop.webapp.entity.ApStock;
import com.shop.webapp.mapper.ApStockMapper;
import com.shop.webapp.service.ApStockService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 库存表，代表库存，秒杀库存等信息 服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class ApStockServiceImpl extends ServiceImpl<ApStockMapper, ApStock> implements ApStockService {

}
