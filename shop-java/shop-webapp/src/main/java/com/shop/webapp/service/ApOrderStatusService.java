package com.shop.webapp.service;

import com.shop.webapp.entity.ApOrderStatus;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单状态表 服务类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApOrderStatusService extends IService<ApOrderStatus> {

}
