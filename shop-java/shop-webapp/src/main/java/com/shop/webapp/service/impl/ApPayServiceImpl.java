package com.shop.webapp.service.impl;

import com.shop.webapp.entity.ApPay;
import com.shop.webapp.mapper.ApPayMapper;
import com.shop.webapp.service.ApPayService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class ApPayServiceImpl extends ServiceImpl<ApPayMapper, ApPay> implements ApPayService {

}
