package com.shop.webapp.mapper;

import com.shop.webapp.entity.ApProduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shop.webapp.entity.ApUser;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8 Mapper 接口
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApProductMapper extends BaseMapper<ApProduct> {

    @Select("SELECT * FROM ap_product WHERE id >= (SELECT floor(RAND() * (SELECT MAX(id) FROM ap_product))) ORDER BY id LIMIT 5")
    public List<ApProduct> findGoodProduct();

    @Select("SELECT * FROM ap_product WHERE id >= (SELECT floor(RAND() * (SELECT MAX(id) FROM ap_product))) ORDER BY id LIMIT 10")
    public List<ApProduct> findRedProduct();

    @Select("SELECT * FROM ap_product WHERE id >= (SELECT floor(RAND() * (SELECT MAX(id) FROM ap_product))) ORDER BY id LIMIT 10")
    public List<ApProduct> findlikeProduct();

    @Select("SELECT * FROM ap_product WHERE id >= (SELECT floor(RAND() * (SELECT MAX(id) FROM ap_product))) ORDER BY id LIMIT 6")
    public List<ApProduct> rollShowProduct();
}
