package com.shop.webapp.controller;


import com.fredomli.common.response.Result;
import com.shop.webapp.service.ApAbstractProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * spu表，该表描述的是一个抽象性的商品，比如 iphone8 前端控制器
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@RestController
@RequestMapping("/webapp/ap-abstract-product")
@Api(value = "虚拟商品模块", tags = "虚拟商品接口")
public class ApAbstractProductController {

    @Autowired
    private ApAbstractProductService productService;

    @ApiOperation("商品列表")
    @GetMapping("get-product-list")
    public Result getAbstractProductList() {
        return Result.ok().data("getAbstractProductList","getAbstractProductList");
    }

    @ApiOperation("单条商品记录")
    @GetMapping("get-one-product")
    public Result getOneAbstractProduct() {
        return Result.ok().data("getOneAbstractProduct","getOneAbstractProduct");
    }

    @ApiOperation("添加虚拟商品记录")
    @PostMapping("add-abstract-product")
    public Result addAbstractProduct() {
        return Result.ok().data("addAbstractProduct","addAbstractProduct");
    }

    @ApiOperation("更新虚拟商品记录")
    @PutMapping("update-abstract-product")
    public Result updateAbstractProduct() {
        return Result.ok().data("updateAbstractProduct","updateAbstractProduct");
    }

    @ApiOperation("删除虚拟商品记录")
    @DeleteMapping("delete-abstract-product")
    public Result deleteAbstractProduct() {
        return Result.ok().data("deleteAbstractProduct","deleteAbstractProduct");
    }


}

