package com.shop.webapp.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 运营管理菜单
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="MpManagementMenu对象", description="运营管理菜单")
public class MpManagementMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "菜单名称")
    private String title;

    @ApiModelProperty(value = "图标")
    private String icon;

    @ApiModelProperty(value = "访问地址")
    private String path;

    @ApiModelProperty(value = "添加时间")
    @TableField("addTime")
    private Date addtime;

    @ApiModelProperty(value = "修改时间")
    @TableField("modifyTime")
    private Date modifytime;

    @ApiModelProperty(value = "状态")
    private Boolean status;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "父id")
    @TableField("parentId")
    private Integer parentid;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "选中高亮显示的id")
    @TableField("selectedId")
    private String selectedid;

    @ApiModelProperty(value = "svg图标")
    private String svg;


}
