package com.shop.webapp.service;

import com.shop.webapp.entity.ApProduct;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * sku表,该表表示具体的商品实体,如黑色的 64g的iphone 8 服务类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
public interface ApProductService extends IService<ApProduct> {

}
