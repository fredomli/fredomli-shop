package com.shop.webapp.service.impl;

import com.shop.webapp.entity.ApAbstractProduct;
import com.shop.webapp.mapper.ApAbstractProductMapper;
import com.shop.webapp.service.ApAbstractProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * spu表，该表描述的是一个抽象性的商品，比如 iphone8 服务实现类
 * </p>
 *
 * @author fredomli
 * @since 2021-01-01
 */
@Service
public class ApAbstractProductServiceImpl extends ServiceImpl<ApAbstractProductMapper, ApAbstractProduct> implements ApAbstractProductService {

}
