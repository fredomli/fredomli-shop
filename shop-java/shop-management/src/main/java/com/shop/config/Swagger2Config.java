package com.shop.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author lishuang
 * @calssName Swagger2Config
 * @description
 * @date 2020/12/30 22:42
 */
@Configuration
@EnableSwagger2
@EnableKnife4j
public class Swagger2Config {
    /**
     * doc: 		http://localhost:8882/doc.html
     * Swagger: 	http://localhost:8882/swagger-ui.html
     *
     */

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
                .apis(RequestHandlerSelectors.basePackage("com.shop.webapp")).paths(PathSelectors.any()).build();
    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("网上商品商城-运营后台")
                .description("网上商品商城接口文档")
                .termsOfServiceUrl("http://localhost:8882/")
                .version("1.0")
                .contact(new Contact("fredomli", "http://localhost:8882/", "fredomli@163.com"))
                .build();
    }
}
