// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'

// 导入NProgress, 包对应的JS和CSS
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

// 引入element-ui配置信息
import '@/plugins/element'
// 引入element-ui样式文件
import 'element-ui/lib/theme-chalk/index.css'
// 导入全局样式
import '@/assets/css/global.css'
// 导入字体图标
import '@/assets/fonts/iconfont.css'

// 配置请求根路径，包括本机地址、远程地址
axios.defaults.baseURL = 'http://www.test.com/'

// 在request拦截器中，展示进度条Nprogress.start()
axios.interceptors.request.use(config => {
  NProgress.start()
  // 为请求头对象，添加token验证的 Authorization 字段
  config.headers.Authorization = window.sessionStorage.getItem('token')
  // 返回config
  return config
})

// response 拦截器中，隐藏进度条 NProgress.done()
axios.interceptors.response.use(config => {
  NProgress.done()
  return config
})

Vue.config.productionTip = false

// 将axios挂载到Vue实例，后面可通过this调用
Vue.prototype.$http = axios
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
