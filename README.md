# fredomli-shop

#### 介绍
该项目是一个多端网上商城项目，包括PC端，移动端，PC端门户系统和管理系统，移动端门户系统和管理系统，包括Web App.
#### 技术栈
##### 前端：
1. JavaScript、CSS、HTML
2. Element-ui
3. Node.js、Npm、Webpack
4. Vue、Vue-router、Vue-cli、Vuex
5. Vant-ui
6. Axios
7. Mock.js
8. Swagger
9. Mysql
10. Mongodb

##### 后端：
1. Java
2. SpringBoot
3. SpringSecurity
4. Mybatis、Mybatis-plus
5. Swagger2
6. Tomcat
7. Maven
8. Mysql

##### 版本管理工具：
1. Git、Gitee
